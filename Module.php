<?php

namespace xolodok\setting;

/**
 * Setting module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'xolodok\setting\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
