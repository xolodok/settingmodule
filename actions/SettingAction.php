<?php

namespace xolodok\setting\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class SettingAction extends Action
{
    public $modelClass;

    public $sectionName;

    public $view = 'settings';

    public $viewParams = [];

    public function init()
    {
        parent::init();

        if($this->modelClass === null){
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    public function run()
    {
        $model = Yii::createObject($this->modelClass);

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $this->saveSettings($model);

            return $this->controller->refresh();
        }

        $this->prepareModel($model);

        return $this->controller->render($this->view, ArrayHelper::merge($this->viewParams, [
            'model' => $model,
        ]));
    }

    protected function saveSettings($model)
    {
        foreach ($model->toArray() as $key => $value){
            Yii::$app->settings->set($this->getSection($model), $key, $value);
        }
    }

    protected function getSection($model)
    {
        if($this->sectionName !== null){
            return $this->sectionName;
        }

        return $model->formName();
    }

    protected function prepareModel(&$model)
    {
        foreach ($model->attributes() as $attribute){
            $value = Yii::$app->settings->get($this->getSection($model), $attribute);

            if(!is_null($value)){
                $model->{$attribute} = $value;
            }
        }
    }
}