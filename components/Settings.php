<?php

namespace xolodok\setting\components;

use Yii;
use yii\base\Component;
use yii\di\Instance;
use yii\caching\Cache;
use yii\helpers\ArrayHelper;

class Settings extends Component
{
    public $modelClass = 'xolodok\setting\models\Setting';

    public $cache = 'cache';

    public $cacheKey = 'settings';

    protected $model;

    protected $items;

    protected $setting;

    public function init()
    {
        parent::init();

        if($this->cache !== null){
            $this->cache = Instance::ensure($this->cache, Cache::className());
        }

        $this->model = Yii::createObject($this->modelClass);
    }

    public function get($section, $key, $default = null)
    {
        $items = $this->getSettingsConfig();

        if(isset($items[$section][$key])){
            $this->setting = ArrayHelper::getValue($items[$section][$key], 'value');
            //$type = ArrayHelper::getValue($items[$section][$key], 'type');
        }
        else{
            $this->setting = $default;
        }

        return $this->setting;
    }

    public function set($section, $key, $value, $type = null)
    {
        if($this->model->setSetting($section, $key, $value, $type)){
            if($this->invalidateCache()){
                return true;
            }
        }

        return false;
    }

    protected function getSettingsConfig()
    {
        if(!$this->cache instanceof Cache){
            $this->items = $this->model->getSettings();
        }
        else{
            $cacheItems = $this->cache->get($this->cacheKey);

            if(!empty($cacheItems)){
                $this->items = $cacheItems;
            }
            else{
                $this->items = $this->model->getSettings();
                $this->cache->set($this->cacheKey, $this->items);
            }
        }

        return $this->items;
    }

    public function invalidateCache()
    {
        if($this->cache !== null){
            $this->cache->delete($this->cacheKey);
            Yii::$app->cacheFrontend->delete($this->cacheKey);
            $this->items = null;
        }

        return true;
    }
}
