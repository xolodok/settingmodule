<?php

use yii\db\Migration;

class m170714_181828_init_settings extends Migration
{
    public $table = '{{%settings}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'section' => $this->string(),
            'key' => $this->string(),
            'value' => $this->text(),
            'active' => $this->integer()->notNull()->defaultValue(1),
            'date_created' => $this->dateTime(),
            'date_updated' => $this->dateTime(),
        ]);

        $this->createIndex('settings_unique_key_section', $this->table, ['section', 'key'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('settings_unique_key_section', $this->table);
        $this->dropTable('settings');
    }
}
